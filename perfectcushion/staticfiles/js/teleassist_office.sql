-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2020 at 02:08 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teleassist_office`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pm_id` bigint(20) UNSIGNED NOT NULL,
  `subtask_id` bigint(20) UNSIGNED NOT NULL,
  `agenda` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spent_hour` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isServiced` int(11) NOT NULL DEFAULT 0,
  `isCancelled` int(11) NOT NULL DEFAULT 0,
  `approvedBy` int(11) NOT NULL DEFAULT 0,
  `meeting_dateTime` datetime(6) NOT NULL,
  `meeting_mins` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clinics`
--

CREATE TABLE `clinics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `days`
--

CREATE TABLE `days` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `moderator_id` bigint(20) UNSIGNED NOT NULL,
  `registration_no` int(11) DEFAULT NULL,
  `educational_degrees` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visit_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isActiveForScheduling` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `doctor_schedules`
--

CREATE TABLE `doctor_schedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `doctor_id` bigint(20) UNSIGNED NOT NULL,
  `day_id` bigint(20) UNSIGNED NOT NULL,
  `entry_time` time NOT NULL,
  `leave_time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `age` int(11) NOT NULL,
  `emergency_contact_person` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emergency_contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `evms`
--

CREATE TABLE `evms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `evms_tdo_id` bigint(20) UNSIGNED NOT NULL,
  `bcws` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pv` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actual_cost` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acwp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bac` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ac` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cv` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pmb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sub_task_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_04_29_085635_create_permissions_table', 1),
(5, '2020_04_29_085647_create_roles_table', 1),
(6, '2020_04_29_090003_create_users_permissions_table', 1),
(7, '2020_04_29_090212_create_users_roles_table', 1),
(8, '2020_04_29_090309_create_roles_permissions_table', 1),
(9, '2020_05_04_153717_create_projects_table', 1),
(10, '2020_05_04_155949_create_wbs_table', 1),
(11, '2020_05_04_174734_create_wbs_hour_calculation_table', 1),
(12, '2020_05_18_074729_create_clinics_table', 1),
(13, '2020_05_18_080621_create_departments_table', 1),
(14, '2020_05_18_084459_create_moderators_table', 1),
(15, '2020_05_18_084460_create_doctors_table', 1),
(16, '2020_05_18_091240_create_employee_table', 1),
(17, '2020_05_18_100832_create_days_table', 1),
(18, '2020_05_18_100951_create_doctor_schedules_table', 1),
(19, '2020_05_18_102125_create_slots_table', 1),
(20, '2020_08_17_075914_add_some_attribute_to_wbs_table', 1),
(21, '2020_08_26_221358_create_tdo', 1),
(22, '2020_08_26_221726_create_sub_task', 1),
(23, '2020_08_26_221754_create_work_package', 1),
(24, '2020_08_26_221838_create_project_plan', 1),
(25, '2020_08_26_2221845_create_project_plan_details', 1),
(26, '2020_08_26_224940_create_wbs_master', 1),
(27, '2020_08_27_070039_create_files_table', 1),
(28, '2020_08_27_104915_create_appointments_table', 1),
(29, '2020_09_03_105205_create_evms', 1),
(30, '2020_09_03_110406_create_wbs_details_duplicate', 1),
(31, '2020_09_07_042939_create_slc', 1);

-- --------------------------------------------------------

--
-- Table structure for table `moderators`
--

CREATE TABLE `moderators` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'dashboard', '2020-09-23 06:07:40', '2020-09-23 06:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_lead_id` bigint(20) UNSIGNED NOT NULL,
  `support_engg_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_client` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_summary` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_file_dir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_start_date` timestamp NULL DEFAULT NULL,
  `project_end_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_plans`
--

CREATE TABLE `project_plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `plan_title` char(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `planned_hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `planned_ep` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_plan_work_package_id` bigint(20) UNSIGNED NOT NULL,
  `project_plan_assignee` bigint(20) UNSIGNED NOT NULL,
  `planned_delivery_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_plan_details`
--

CREATE TABLE `project_plan_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_task_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_plan_project_plan_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'super-admin', '2020-09-23 06:07:40', '2020-09-23 06:07:40'),
(2, 'Moderator', 'admin', '2020-09-23 06:07:40', '2020-09-23 06:07:40'),
(3, 'PM', 'power-user', '2020-09-23 06:07:40', '2020-09-23 06:07:40'),
(4, 'employee', 'user', '2020-09-23 06:07:40', '2020-09-23 06:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `roles_permissions`
--

CREATE TABLE `roles_permissions` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles_permissions`
--

INSERT INTO `roles_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slc`
--

CREATE TABLE `slc` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_rate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hourly_rate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `planned_hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `planned_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ep` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slots`
--

CREATE TABLE `slots` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_tasks`
--

CREATE TABLE `sub_tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sub_task_name` char(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtask_tdo_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tdos`
--

CREATE TABLE `tdos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `active_by` tinyint(4) NOT NULL DEFAULT 0,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `email`, `image`, `gender`, `age`, `is_active`, `active_by`, `email_verified_at`, `password`, `remember_token`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Golam Kibria Papel', '01780208855', 'admin@gmail.com', NULL, 'Male', '30', 1, 0, NULL, '$2y$10$RT1uez//Qf3ClvhRcWIhJ.PuLEymdzfVmeQDCPT0g//Eadf9CGj.G', NULL, 0, '2020-09-23 06:07:40', '2020-09-23 06:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wbs`
--

CREATE TABLE `wbs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wbs_user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `wbs_project_id` bigint(20) UNSIGNED DEFAULT NULL,
  `wbs_task_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wbs_task_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `wbs_file_dir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `task_start_date` date DEFAULT NULL,
  `task_end_date` date DEFAULT NULL,
  `task_comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `actual_hours_worked` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actual_hours_today` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_deliverable` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wbs_details_duplicate`
--

CREATE TABLE `wbs_details_duplicate` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wbs_details_duplicate_employee_id` bigint(20) UNSIGNED NOT NULL,
  `wbs_task_details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wbs_duplicate_wbs_master_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wbs_hour_calculation`
--

CREATE TABLE `wbs_hour_calculation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wbs_id` bigint(20) UNSIGNED NOT NULL,
  `wbs_user_id` bigint(20) UNSIGNED NOT NULL,
  `wbs_date` date DEFAULT NULL,
  `wbs_hour` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wbs_masters`
--

CREATE TABLE `wbs_masters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wbs_master_assignee_id` bigint(20) UNSIGNED NOT NULL,
  `wbs_master_project_plan_details_id` bigint(20) UNSIGNED NOT NULL,
  `wbs_master_wbs_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `work_packages`
--

CREATE TABLE `work_packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `work_package_number` bigint(20) NOT NULL,
  `work_package_subtask_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appointments_pm_id_foreign` (`pm_id`),
  ADD KEY `appointments_subtask_id_foreign` (`subtask_id`);

--
-- Indexes for table `clinics`
--
ALTER TABLE `clinics`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clinics_name_unique` (`name`),
  ADD UNIQUE KEY `clinics_phone_unique` (`phone`),
  ADD UNIQUE KEY `clinics_email_unique` (`email`);

--
-- Indexes for table `days`
--
ALTER TABLE `days`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `days_name_unique` (`name`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_department_name_unique` (`department_name`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doctors_user_id_foreign` (`user_id`),
  ADD KEY `doctors_department_id_foreign` (`department_id`),
  ADD KEY `doctors_moderator_id_foreign` (`moderator_id`);

--
-- Indexes for table `doctor_schedules`
--
ALTER TABLE `doctor_schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doctor_schedules_doctor_id_foreign` (`doctor_id`),
  ADD KEY `doctor_schedules_day_id_foreign` (`day_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_user_id_foreign` (`user_id`);

--
-- Indexes for table `evms`
--
ALTER TABLE `evms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evms_evms_tdo_id_foreign` (`evms_tdo_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_sub_task_id_foreign` (`sub_task_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moderators`
--
ALTER TABLE `moderators`
  ADD PRIMARY KEY (`id`),
  ADD KEY `moderators_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projects_project_lead_id_foreign` (`project_lead_id`);

--
-- Indexes for table `project_plans`
--
ALTER TABLE `project_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_plans_project_plan_work_package_id_foreign` (`project_plan_work_package_id`),
  ADD KEY `project_plans_project_plan_assignee_foreign` (`project_plan_assignee`);

--
-- Indexes for table `project_plan_details`
--
ALTER TABLE `project_plan_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_plan_details_project_plan_project_plan_id_foreign` (`project_plan_project_plan_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD PRIMARY KEY (`role_id`,`permission_id`),
  ADD KEY `roles_permissions_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `slc`
--
ALTER TABLE `slc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slots`
--
ALTER TABLE `slots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_tasks`
--
ALTER TABLE `sub_tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_tasks_subtask_tdo_id_foreign` (`subtask_tdo_id`);

--
-- Indexes for table `tdos`
--
ALTER TABLE `tdos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tdos_created_by_foreign` (`created_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`user_id`,`permission_id`),
  ADD KEY `users_permissions_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `users_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `wbs`
--
ALTER TABLE `wbs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wbs_wbs_user_id_foreign` (`wbs_user_id`),
  ADD KEY `wbs_wbs_project_id_foreign` (`wbs_project_id`);

--
-- Indexes for table `wbs_details_duplicate`
--
ALTER TABLE `wbs_details_duplicate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wbs_details_duplicate_wbs_details_duplicate_employee_id_foreign` (`wbs_details_duplicate_employee_id`),
  ADD KEY `wbs_details_duplicate_wbs_duplicate_wbs_master_id_foreign` (`wbs_duplicate_wbs_master_id`);

--
-- Indexes for table `wbs_hour_calculation`
--
ALTER TABLE `wbs_hour_calculation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wbs_hour_calculation_wbs_id_foreign` (`wbs_id`),
  ADD KEY `wbs_hour_calculation_wbs_user_id_foreign` (`wbs_user_id`);

--
-- Indexes for table `wbs_masters`
--
ALTER TABLE `wbs_masters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wbs_masters_wbs_master_assignee_id_foreign` (`wbs_master_assignee_id`),
  ADD KEY `wbs_masters_wbs_master_project_plan_details_id_foreign` (`wbs_master_project_plan_details_id`),
  ADD KEY `wbs_masters_wbs_master_wbs_id_foreign` (`wbs_master_wbs_id`);

--
-- Indexes for table `work_packages`
--
ALTER TABLE `work_packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `work_packages_work_package_number_unique` (`work_package_number`),
  ADD KEY `work_packages_work_package_subtask_id_foreign` (`work_package_subtask_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clinics`
--
ALTER TABLE `clinics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `days`
--
ALTER TABLE `days`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `doctor_schedules`
--
ALTER TABLE `doctor_schedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evms`
--
ALTER TABLE `evms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `moderators`
--
ALTER TABLE `moderators`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_plans`
--
ALTER TABLE `project_plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project_plan_details`
--
ALTER TABLE `project_plan_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slc`
--
ALTER TABLE `slc`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slots`
--
ALTER TABLE `slots`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_tasks`
--
ALTER TABLE `sub_tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tdos`
--
ALTER TABLE `tdos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wbs`
--
ALTER TABLE `wbs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wbs_details_duplicate`
--
ALTER TABLE `wbs_details_duplicate`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wbs_hour_calculation`
--
ALTER TABLE `wbs_hour_calculation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wbs_masters`
--
ALTER TABLE `wbs_masters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `work_packages`
--
ALTER TABLE `work_packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `appointments_pm_id_foreign` FOREIGN KEY (`pm_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `appointments_subtask_id_foreign` FOREIGN KEY (`subtask_id`) REFERENCES `sub_tasks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `doctors`
--
ALTER TABLE `doctors`
  ADD CONSTRAINT `doctors_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `doctors_moderator_id_foreign` FOREIGN KEY (`moderator_id`) REFERENCES `moderators` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `doctors_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `doctor_schedules`
--
ALTER TABLE `doctor_schedules`
  ADD CONSTRAINT `doctor_schedules_day_id_foreign` FOREIGN KEY (`day_id`) REFERENCES `days` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `doctor_schedules_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `evms`
--
ALTER TABLE `evms`
  ADD CONSTRAINT `evms_evms_tdo_id_foreign` FOREIGN KEY (`evms_tdo_id`) REFERENCES `tdos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_sub_task_id_foreign` FOREIGN KEY (`sub_task_id`) REFERENCES `sub_tasks` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `moderators`
--
ALTER TABLE `moderators`
  ADD CONSTRAINT `moderators_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_project_lead_id_foreign` FOREIGN KEY (`project_lead_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project_plans`
--
ALTER TABLE `project_plans`
  ADD CONSTRAINT `project_plans_project_plan_assignee_foreign` FOREIGN KEY (`project_plan_assignee`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_plans_project_plan_work_package_id_foreign` FOREIGN KEY (`project_plan_work_package_id`) REFERENCES `work_packages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project_plan_details`
--
ALTER TABLE `project_plan_details`
  ADD CONSTRAINT `project_plan_details_project_plan_project_plan_id_foreign` FOREIGN KEY (`project_plan_project_plan_id`) REFERENCES `project_plans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD CONSTRAINT `roles_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `roles_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_tasks`
--
ALTER TABLE `sub_tasks`
  ADD CONSTRAINT `sub_tasks_subtask_tdo_id_foreign` FOREIGN KEY (`subtask_tdo_id`) REFERENCES `tdos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tdos`
--
ALTER TABLE `tdos`
  ADD CONSTRAINT `tdos_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD CONSTRAINT `users_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD CONSTRAINT `users_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wbs`
--
ALTER TABLE `wbs`
  ADD CONSTRAINT `wbs_wbs_project_id_foreign` FOREIGN KEY (`wbs_project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wbs_wbs_user_id_foreign` FOREIGN KEY (`wbs_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wbs_details_duplicate`
--
ALTER TABLE `wbs_details_duplicate`
  ADD CONSTRAINT `wbs_details_duplicate_wbs_details_duplicate_employee_id_foreign` FOREIGN KEY (`wbs_details_duplicate_employee_id`) REFERENCES `employee` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wbs_details_duplicate_wbs_duplicate_wbs_master_id_foreign` FOREIGN KEY (`wbs_duplicate_wbs_master_id`) REFERENCES `wbs_masters` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wbs_hour_calculation`
--
ALTER TABLE `wbs_hour_calculation`
  ADD CONSTRAINT `wbs_hour_calculation_wbs_id_foreign` FOREIGN KEY (`wbs_id`) REFERENCES `wbs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wbs_hour_calculation_wbs_user_id_foreign` FOREIGN KEY (`wbs_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wbs_masters`
--
ALTER TABLE `wbs_masters`
  ADD CONSTRAINT `wbs_masters_wbs_master_assignee_id_foreign` FOREIGN KEY (`wbs_master_assignee_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wbs_masters_wbs_master_project_plan_details_id_foreign` FOREIGN KEY (`wbs_master_project_plan_details_id`) REFERENCES `project_plan_details` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wbs_masters_wbs_master_wbs_id_foreign` FOREIGN KEY (`wbs_master_wbs_id`) REFERENCES `wbs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `work_packages`
--
ALTER TABLE `work_packages`
  ADD CONSTRAINT `work_packages_work_package_subtask_id_foreign` FOREIGN KEY (`work_package_subtask_id`) REFERENCES `sub_tasks` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
